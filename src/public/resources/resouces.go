package resources

import "RetestCRUD/adapter/models"

type Order struct {
	Id        int    `json:"id"`
	OwnerID   int    `json:"owner_id"`
	Name      string `json:"name"`
	Address   string `json:"address"`
	CreatedAt int64  `json:"created_at"`
	UpdatedAt int64  `json:"updated_at"`
}

func NewOrder(entity *models.Restaurants) *Order {
	return &Order{
		Id:        entity.Id,
		OwnerID:   entity.OwnerID,
		Name:      entity.Name,
		Address:   entity.Address,
		CreatedAt: entity.CreatedAt.Unix(),
		UpdatedAt: entity.UpdatedAt.Unix(),
	}
}
