package bootstrap

import (
	"RetestCRUD/adapter/repositories"
	"RetestCRUD/public/controller"
	"RetestCRUD/public/properties"
	"RetestCRUD/public/router"
	"RetestCRUD/public/service"
	"gitlab.com/golibs-starter/golib"
	"go.uber.org/fx"
)

func All() fx.Option {
	return fx.Options(
		golib.AppOpt(),
		golib.PropertiesOpt(),
		golib.LoggingOpt(),
		golib.EventOpt(),
		golib.BuildInfoOpt(Version, CommitHash, BuildTime),
		golib.ActuatorEndpointOpt(),
		golib.ProvideProps(properties.NewSwaggerProperties),
		fx.Provide(repositories.NewAppRepository),
		fx.Provide(controller.NewOrderController),
		fx.Provide(service.NewConnectedAppServices),
		fx.Invoke(router.RegisterHandlers),
		fx.Invoke(router.RegisterGinRouters),
	)
}
