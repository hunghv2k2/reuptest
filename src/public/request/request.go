package requests

type GetOwnerIDRequest struct {
	OwnerID int
}

type CreateRequest struct {
	OwnerID int
	Name    string
	Address string
}

type UpdateRequest struct {
	OwnerID int
	Name    string
	Address string
}

type DeleteRequest struct {
	OwnerID int
}
