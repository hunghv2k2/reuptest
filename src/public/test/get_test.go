package tests

import (
	"RetestCRUD/adapter/models"
	golibdataTestUtil "gitlab.com/golibs-starter/golib-data/testutil"
	golibtest "gitlab.com/golibs-starter/golib-test"
	"net/http"
	"testing"
)

func TestGetRestaurant_ShouldReturnSuccess(t *testing.T) {
	golibdataTestUtil.TruncateTablesOpt("restaurants")
	data := []*models.Restaurants{
		{
			Id:      1,
			Name:    "resabc1",
			Address: "hanoi",
			OwnerID: 1,
		},
		{
			Id:      3,
			Name:    "resabc3",
			Address: "haiphong",
			OwnerID: 3,
		},
		{
			Id:      4,
			Name:    "resabc4",
			Address: "quangninh",
			OwnerID: 4,
		},
	}

	golibdataTestUtil.Insert(data)
	golibtest.NewRestAssured(t).
		When().
		Get("/v1/orders/:id").
		Then().
		Status(http.StatusOK).
		Body("meta.code", 200).
		Body("data.#", 2).
		Body("data.0.id", 1).
		Body("data.0.name", "resabc1").
		Body("data.0.addr", "hanoi").
		Body("data.0.owner_id", 1).
		Body("data.1.id", 3).
		Body("data.1.name", "resabc3").
		Body("data.1.addr", "haiphong").
		Body("data.1.owner_id", 3)
}
