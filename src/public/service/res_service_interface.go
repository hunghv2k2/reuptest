package service

import (
	"RetestCRUD/adapter/models"
	requests "RetestCRUD/public/request"
	"context"
)

type ConnectedAppServicesInterface interface {
	GetByFilter(ctx context.Context, req *requests.GetOwnerIDRequest) (*models.Restaurants, error)
	CreateData(ctx context.Context, req *requests.CreateRequest) error
	DeleteData(ctx context.Context, req *requests.DeleteRequest) error
	UpdateData(ctx context.Context, req *requests.UpdateRequest) error
}
