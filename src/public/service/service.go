package service

import (
	"RetestCRUD/adapter/models"
	"RetestCRUD/adapter/repositories"
	requests "RetestCRUD/public/request"
	"context"
	golibcache "gitlab.com/golibs-starter/golib-cache"
	"gitlab.com/golibs-starter/golib/log"
	"go.uber.org/fx"
)

type ConnectedAppServices struct {
	params ConnectedAppServicesConstructorParams
}

type ConnectedAppServicesConstructorParams struct {
	fx.In
	AppRepository   repositories.AppRepositoryInterface
	Cache           *golibcache.Cache
	CacheProperties *golibcache.CacheProperties
}

func NewConnectedAppServices(params ConnectedAppServicesConstructorParams) ConnectedAppServicesInterface {
	return &ConnectedAppServices{params: params}
}

func (a *ConnectedAppServices) GetByFilter(ctx context.Context, req *requests.GetOwnerIDRequest) (*models.Restaurants, error) {
	oldata, errs := a.params.AppRepository.GetAllData(ctx, req.OwnerID)

	if errs != nil {
		log.Fatalf("Can't found data", errs)
	}
	filter := &models.AppFilter{OwnerID: req.OwnerID, Status: oldata.Status}

	data, err := a.params.AppRepository.GetByFilter(ctx, filter)
	if err != nil {
		return nil, err
	}
	return data, nil
}

func (a *ConnectedAppServices) CreateData(ctx context.Context, req *requests.CreateRequest) error {
	//_, errs := a.params.AppRepository.GetAllData(ctx, req.OwnerID)
	//
	//if errs == nil {
	//	errors.New("OwnerID have been used")
	//}

	data := &models.AppCreate{OwnerID: req.OwnerID, Name: req.Name, Address: req.Address}
	err := a.params.AppRepository.CreateOrder(ctx, data)

	if err != nil {
		return err
	}
	return nil
}

func (a *ConnectedAppServices) DeleteData(ctx context.Context, req *requests.DeleteRequest) error {
	oldata, err := a.params.AppRepository.GetAllData(ctx, req.OwnerID)

	if err != nil {
		return err
	}

	if oldata.Status == 0 {
		log.Fatalf("Data not found")
	}

	data := &models.AppFilter{OwnerID: req.OwnerID, Status: oldata.Status}

	if err := a.params.AppRepository.DeleteByFilter(ctx, data); err != nil {
		return err
	}
	return nil
}

func (a *ConnectedAppServices) UpdateData(ctx context.Context, req *requests.UpdateRequest) error {
	oldata, err := a.params.AppRepository.GetAllData(ctx, req.OwnerID)

	if err != nil {
		return err
	}

	filter := &models.AppFilter{OwnerID: req.OwnerID, Status: oldata.Status}
	data := &models.AppUpdate{Name: req.Name, Address: req.Address}

	if err := a.params.AppRepository.UpdateOrder(ctx, data, filter); err != nil {
		return err
	}
	return nil
}
