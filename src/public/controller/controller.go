package controller

import (
	"RetestCRUD/public/request"
	"RetestCRUD/public/resources"
	"RetestCRUD/public/service"
	"github.com/gin-gonic/gin"
	baseEx "gitlab.com/golibs-starter/golib/exception"
	"gitlab.com/golibs-starter/golib/web/log"
	"gitlab.com/golibs-starter/golib/web/response"
)

type OrderController struct {
	orderServices service.ConnectedAppServicesInterface
}

func NewOrderController(orderServices service.ConnectedAppServicesInterface) *OrderController {
	return &OrderController{orderServices: orderServices}
}

func (a *OrderController) GetRestaurant(ctx *gin.Context) {
	var req requests.GetOwnerIDRequest
	if err := ctx.ShouldBind(&req); err != nil {
		log.Error(ctx, "Cannot bind request, err [%s]", err)
		response.WriteError(ctx.Writer, baseEx.BadRequest)
		return
	}

	entity, err := a.orderServices.GetByFilter(ctx.Request.Context(), &req)
	if err != nil {
		response.WriteError(ctx.Writer, err)
		return
	}
	response.Write(ctx.Writer, response.Ok(resources.NewOrder(entity)))
}

func (a *OrderController) CreateRestaurant(ctx *gin.Context) {
	var req requests.CreateRequest
	if err := ctx.ShouldBind(&req); err != nil {
		log.Error(ctx, "Cannot bind request, err [%s]", err)
		response.WriteError(ctx.Writer, baseEx.BadRequest)
		return
	}

	err := a.orderServices.CreateData(ctx.Request.Context(), &req)
	if err != nil {
		response.WriteError(ctx.Writer, err)
		return
	}
	response.Write(ctx.Writer, response.Ok(req))
}

func (a *OrderController) UpdateRestaurant(ctx *gin.Context) {
	var req requests.UpdateRequest
	if err := ctx.BindQuery(&req); err != nil {
		log.Error(ctx, "Cannot bind request, err [%s]", err)
		response.WriteError(ctx.Writer, baseEx.BadRequest)
		return
	}

	err := a.orderServices.UpdateData(ctx.Request.Context(), &req)
	if err != nil {
		response.WriteError(ctx.Writer, err)
		return
	}
	response.Write(ctx.Writer, response.Ok(req))
}

func (a *OrderController) DeleteRestaurant(ctx *gin.Context) {
	var req requests.DeleteRequest
	if err := ctx.BindQuery(&req); err != nil {
		log.Error(ctx, "Cannot bind request, err [%s]", err)
		response.WriteError(ctx.Writer, baseEx.BadRequest)
		return
	}

	err := a.orderServices.DeleteData(ctx.Request.Context(), &req)
	if err != nil {
		response.WriteError(ctx.Writer, err)
		return
	}
	response.Write(ctx.Writer, response.Ok(1))
}
