package public

import (
	"go.uber.org/fx"
	"public/bootstrap"
)

func main() {
	fx.New(bootstrap.All()).Run()
}
