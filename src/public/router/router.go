package router

import (
	"RetestCRUD/public/controller"
	"RetestCRUD/public/properties"
	"github.com/gin-gonic/gin"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
	"github.com/swaggo/swag/example/basic/docs"
	"gitlab.com/golibs-starter/golib"
	"gitlab.com/golibs-starter/golib-gin"
	"gitlab.com/golibs-starter/golib/web/actuator"
	"go.uber.org/fx"
)

type RegisterRoutersIn struct {
	fx.In
	App             *golib.App
	Engine          *gin.Engine
	SwaggerProps    *properties.SwaggerProperties
	Actuator        *actuator.Endpoint
	OrderController *controller.OrderController
}

// RegisterHandlers register handlers
func RegisterHandlers(app *golib.App, engine *gin.Engine) {
	engine.Use(golibgin.InitContext())
	engine.Use(golibgin.WrapAll(app.Handlers())...)
}

// RegisterGinRouters register gin routes
func RegisterGinRouters(p RegisterRoutersIn) {
	group := p.Engine.Group(p.App.Path())
	group.GET("/actuator/health", gin.WrapF(p.Actuator.Health))
	group.GET("/actuator/info", gin.WrapF(p.Actuator.Info))

	if p.SwaggerProps.Enabled {
		docs.SwaggerInfo.BasePath = p.App.Path()
		group.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
	}

	group.GET("/v1/orders/:id", p.OrderController.GetRestaurant)
	group.POST("/v1/orders", p.OrderController.CreateRestaurant)
	group.DELETE("/v1/orders/:id", p.OrderController.DeleteRestaurant)
	group.PATCH("/v1/orders/:id", p.OrderController.UpdateRestaurant)
}
