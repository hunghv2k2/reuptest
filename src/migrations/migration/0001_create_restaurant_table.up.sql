CREATE TABLE IF NOT EXISTS `restaurants`
(
    id         int auto_increment primary key                                  not null,
    owner_id   int                                                             not null,
    name       varchar(50)                                                     not null,
    addr       varchar(255)                                                    not null,
    lat        double                                                          null,
    lng        double                                                          null,
    status     int       default 1                                             not null,
    created_at timestamp default current_timestamp                             null,
    updated_at timestamp default current_timestamp on update current_timestamp null
);