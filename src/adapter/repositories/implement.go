package repositories

import (
	"RetestCRUD/adapter/models"
	"RetestCRUD/adapter/repositories/scopes"
	"context"
)

func (a *AppRepository) GetByFilter(ctx context.Context, req *models.AppFilter) (*models.Restaurants, error) {
	var data models.Restaurants
	if tx := a.db.Scopes(scopes.WithAppPredicates(req)).First(data); tx.Error != nil {
		return nil, tx.Error
	}
	return &data, nil
}

func (a *AppRepository) CreateOrder(ctx context.Context, req *models.AppCreate) error {
	if err := a.db.Create(&req).Error; err != nil {
		return err
	}

	return nil
}

func (a *AppRepository) DeleteByFilter(ctx context.Context, req *models.AppFilter) error {
	if err := a.db.Scopes(scopes.WithAppPredicates(req)).Updates(req.Status == 0).Error; err != nil {
		return err
	}

	return nil
}

func (a *AppRepository) UpdateOrder(ctx context.Context, data *models.AppUpdate, req *models.AppFilter) error {
	if err := a.db.Scopes(scopes.WithAppPredicates(req)).Updates(&data).Error; err != nil {
		return err
	}

	return nil
}

func (a *AppRepository) GetAllData(ctx context.Context, OwnerID int) (*models.Restaurants, error) {
	var data models.Restaurants
	if err := a.db.Where("owner_id", OwnerID).First(&data).Error; err != nil {
		return nil, err
	}
	return &data, nil
}
