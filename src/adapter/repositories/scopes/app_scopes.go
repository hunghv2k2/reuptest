package scopes

import (
	"RetestCRUD/adapter/models"
	"gorm.io/gorm"
)

func WithAppPredicates(req *models.AppFilter) func(db *gorm.DB) *gorm.DB {
	return func(db *gorm.DB) *gorm.DB {

		if req.Status != 0 {
			db.Where("status = ?", req.Status)
		}

		if req.OwnerID != 0 {
			db.Where("owner_id = ?", req.OwnerID)
		}

		return db
	}
}
