package repositories

import (
	"RetestCRUD/adapter/models"
	"context"
)

type AppRepositoryInterface interface {
	GetByFilter(ctx context.Context, req *models.AppFilter) (*models.Restaurants, error)
	CreateOrder(ctx context.Context, req *models.AppCreate) error
	DeleteByFilter(ctx context.Context, req *models.AppFilter) error
	UpdateOrder(ctx context.Context, data *models.AppUpdate, req *models.AppFilter) error
	GetAllData(ctx context.Context, OwnerID int) (*models.Restaurants, error)
}
