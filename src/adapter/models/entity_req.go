package models

type AppFilter struct {
	OwnerID int
	Status  int
}

type AppCreate struct {
	OwnerID int
	Name    string
	Address string
}

type AppUpdate struct {
	Name    string
	Address string
}
