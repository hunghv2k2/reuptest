package models

import "time"

type Restaurants struct {
	Id        int
	OwnerID   int
	Name      string
	Address   string
	Status    int
	CreatedAt *time.Time
	UpdatedAt *time.Time
}

func (Restaurants) TableName() string { return "restaurants" }
